// imports
const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes.js');

// for the configuration of the commands
const titleOptions = {
  describe: 'Title of note',
  demand: true,// is mandatory to use it
  alias: 't'// to use instead of the command (-t="new title")
};

const bodyOptions = {
  describe: 'Body of the note',
  demand: true,
  alias: 'b'
};

// Commands configuration
const argv = yargs
  .command('add', 'Add a new note', {
    // Here we put the arguments of the command
    title: titleOptions,
    body: bodyOptions
  })
  .command('list', 'List all notes')
  .command('read', 'Read an individual note', {
    title: titleOptions
  })
  .command('remove', 'Remove a note', {
    title: titleOptions
  })
  // node app.js --help     (it shows you the command options that we have in the app)
  .help()
  .argv;

// Command got by the user input
var command = argv._[0];

// commands to use in the file
if (command === 'add') {
  	// node app.js add --title=secret --body="Somebody"
  	// arguments that I am sending
  	var note = notes.addNote(argv.title, argv.body);
  	if (note) {
  		console.log('Note created!');
  		notes.printNote(note);
  	} else {
  		console.log('Note title taken');
  	}
} else if (command === 'list') {
 	var allNotes = notes.getAll();
  console.log('Printing', allNotes.length, 'note(s).');
  // printing each note
  allNotes.forEach((note) => notes.printNote(note));
} else if (command === 'read') {
  	var note = notes.getNote(argv.title);

    if (note) {
      console.log('Note data:');
      notes.printNote(note);
    } else {
      console.log('Note not found');
    }
} else if (command === 'remove') {
  	var noteRemoved = notes.removeNote(argv.title);
    var message = noteRemoved ? 'Note Removed!' : 'Note not found';
    console.log(message);
} else {
  	console.log('Command not recognized');
}
