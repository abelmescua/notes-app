var person = {
  name: 'Andrew'
};

person.age = 25;

// to execute the debugger in a specific place of my program
debugger;

person.name = 'Mike';

console.log(person);
