// var square = (x) => x * x;
var square = x => x * x;
console.log(square(9));

var user = {
  name: 'Andrew',
  // ES5 (arrow function)
  // Not recommended for functinos inside objects
  sayHi: () => {
    // console.log(arguments);
    console.log(`Hi. I'm ${this.name}`);
  },
  // ES6 (Regular function): It's flexible, you can send arguments without expecting them.
  // Recommended for functions inside objects
  sayHiAlt () {
    console.log(arguments);
    console.log(`Hi. I'm ${this.name}`);
  }
};

user.sayHi();
user.sayHiAlt(1, 2, 3);
